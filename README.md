# SUM Coupon Tracker Script Documentation

This script is to help [SUM](https://www.sum.ae/) track the usage of their coupons on other websites by using Google Analytics to report the purchases that SUM's coupons are used in. The script will run after purchasing the products.

To be able to run the script, some details are needed:

## Function params

This function `sendCouponUsageToSUM` takes 4 params:

- List of products: an array of the purchased products, each product is an object that contains (product ID, product name, price, quantity)
- Transaction ID
- Coupon code: SUM's coupon code (static)
- Website name: for each website, this should always be the same value (static)

## Installation
- Put the script in the thank you page (you won't have to change the script)
- Check if SUM's coupon is used in the purchase.
- If SUM's coupon is used, prepare the products array of objects, the transaction ID, SUM's coupon code (used) and the website name.
- Call the function `sendCouponUsageToSum` with the needed params.
- Look at the example below


## Example

```
<!DOCTYPE html>
<html>
<script>
// Runs on thank you page load:
  const couponCode = 'SUMSUMMER21';   // Dynamic. Depends on SUM's coupon code that is used in the transation.
  if (couponCode === 'SUMSUMMER21'){  // SUM's coupon code (to access the if statement only if SUM's coupon code is used)
    const products = [
      { id: 'P5453', name: 'Adidas T-Shirt 1', price: 30, quantity: 2 },
      { id: 'P3467', name: 'Nike Running Short 360', price 45, quantity: 1 },
    ];
    const transactionId = 'T54356';     // Dynamic. Depends on the purchase order ID.
    const websiteName = 'Mega Shop'          // Static for each website (Website name)
    sendCouponUsageToSUM(products, transactionId, couponCode, websiteName)
  }
</script>
<body>
	<h1>Thank you for your purchase </h1>
</body>
</html>
```